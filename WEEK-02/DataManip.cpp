#include<iostream>
#include<string>
#include<iomanip>
using namespace std;
int main()
{
    string n;
    float a = 58.32675356363;
     cout<<"what's your name?"<<endl;
    cin>>ws;//ws will ignore the blank spaces that are entered by the user before input value
    cin>>n;
    cout<<"Name is "<<n<<endl;
    cout << setprecision(2) << a << endl;//gives 2 digits as output ignoring decimal points
    cout << setprecision(5) << a << endl;//gives 5 digits as output including decimal points
    cout<<setw(50)<<"white spaces over bro"<<endl;//50 white spaces 
    cout <<setw(15) << setfill('*') << 99 << 97 << endl;//15 *s
   
    cout<<"hey languages"<<flush;// flush will flush the next line same as ends
    cout<<"Hii"<<endl;//endl is used to end the line on cout and print the next output from the next line..
    cout<<"how are you"<<ends;//ends is used to skip next line and continues to print the next outputs immediately
    cout<<"bye"<<endl;
    
}
