import java.lang.*;
import java.util.*;

class job extends Exception {
    job(String s) {
        super(s);
    }

}

class UserExHandJava {
    private static int a;

    static void validate() throws job {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the weekday number:");
        a = sc.nextInt();
        if (a < 7)
            throw new job("it's a working day");
        else
            System.out.println("holiday");

    }

    public static void main(String[] args) {
        try {
            validate();
        } catch (Exception e) {
            System.out.println("exception arised:\n" + e);
        } finally {
            System.out.println("Executed");
        }
    }
}
