class Bikes {
    void mileage() {
        System.out.println(" vehicle with good mileage is ");
    }
}

class Bajaj extends Bikes {
    void platina() {
        System.out.println("bajaj Platina");
    }
}

class SimpleInheriJava {
    public static void main(String[] args) {
        Bajaj d = new Bajaj();
        d.mileage();
        d.platina();
    }
}
