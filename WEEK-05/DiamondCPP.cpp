#include<iostream>
using namespace std;
class person
{
    public:
    person()
    {
        cout<<"the person is called"<<endl;
    }
};
class father:virtual public person
{
    public:
    father()
    {
        cout<<"the father is called"<<endl;
    }
};
class mother: virtual public person
{
    public:
    mother()
    {
        cout<<"the mother is called"<<endl;
    }
};
class child:public father,public mother
{
    public:
    child()
    {
        cout<<"the child is called"<<endl;
    }
};
int main()
{
    child obj;
    return 0;
}
