#include<iostream>
using namespace std;
class open
{
    public:
        void method1()
        {
            cout<<"i'm only the displayed one here"<<endl;
        }
};
class close
{
    void method2()
    {
        cout<<"test case1"<<endl;
    }
};

class secure
{
    protected:
        void method3()
        {
            cout<<"test case2"<<endl;
        }
};
int main()
{
    open obj1;
    close obj2;
    secure obj3;
    cout<<"Public"<<endl;
    obj1.method1();
    cout<<"private"<<endl;
    obj2.method2();
    cout<<"protected"<<endl;
    obj3.method3();
    return 0;
}
