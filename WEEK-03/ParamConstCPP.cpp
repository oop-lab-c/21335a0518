#include<iostream>
using namespace std;
class Student
{
    string Name;
    double Percentage;
    public:
    string collegeName;
    int collegeCode;
    Student()
    {
        collegeName="MVGR";
        collegeCode=33;
    }
    Student(string fullName,double semPercentage)
    {
        Name=fullName;
        Percentage=semPercentage;
    }
    
    void display1()
    {
        cout<<"College Name : "<<collegeName<<endl;
        cout<<"College Code : "<<collegeCode<<endl;
    }
    void display2()
    {
        cout<<"Full Name : "<<Name<<endl;
        cout<<"Sem Percentage : "<<Percentage<<endl;
    }

};
int main()
{
    cout<<"Enter your Full Name :";
    string fullName;
    cin>>fullName;
    cout<<"Enter your Sem Percentage : ";
    double semPercentage;
    cin>>semPercentage;
    Student obj1;
    obj1.display1();
    Student obj2(fullName,semPercentage);
    obj2.display2();
    return 0;
}
