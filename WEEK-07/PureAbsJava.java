import java.util.*;

interface sample {
    void show();

}

class PureAbsJava implements sample {
    public void show() {
        System.out.println("welcome to java");
    }

    public static void main(String[] args) {
        PureAbsJava h = new PureAbsJava();
        h.show();

    }

}
