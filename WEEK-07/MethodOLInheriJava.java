import java.util.*;

class name {
    void show(String name) {

        System.out.println("I'm " + name);
    }
}

class me extends name {
    void show(String name, String n) {
        System.out.println("I'm " + name + " also Known as " + n);
    }
}

public class MethodOLInheriJava {
    public static void main(String[] args) {
        me b = new me();
        b.show("Prem");
        b.show("Prem", "Bharath");

    }

}
