import java.util.Scanner;

class CalcJava {
    public static void main(String[] args) {
        System.out.println("enter two numbers: ");
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        System.out.println("enter a operator");
        char c = in.next().charAt(0);
        if (c == '+') {
            System.out.println(a + b);
        } else if (c == '-') {
            System.out.println(a - b);
        } else if (c == '*') {
            System.out.println(a * b);
        } else if (c == '/') {
            System.out.println(a / b);
        } else if (c == '%') {
            System.out.println(a % b);
        } else {
            System.out.println("enter a valid option");
        }
    }
}
