import java.util.Scanner;

class EvenOddJava {
    public static void main(String[] args) {
        System.out.println("Enter an integer:");
        Scanner input = new Scanner(System.in);
        int x = input.nextInt();
        if (x % 2 == 0)
            System.out.println(x + " is even");
        else
            System.out.println(x + " is odd");
    }
}
