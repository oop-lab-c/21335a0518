import java.util.Scanner;

class HelloUserJava {
    public static void main(String[] args) {
        System.out.println("enter a name: ");

        Scanner in = new Scanner(System.in);
        String name = in.nextLine();
        System.out.println("hello " + name);
    }
}
